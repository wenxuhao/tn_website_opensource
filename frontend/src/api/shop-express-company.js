import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getExpressCompanyList(params) {
  return request({
    url: api_prefix + 'shop_express_company/list',
    method: 'get',
    params
  })
}

export function getExpressCompanyByID(id) {
  return request({
    url: api_prefix + 'shop_express_company/get_id',
    method: 'get',
    params: { id }
  })
}

export function getAllExpressCompanyName(params) {
  return request({
    url: api_prefix + 'shop_express_company/get_all_name',
    method: 'get',
    params
  })
}

export function addExpressCompany(data) {
  return request({
    url: api_prefix + 'shop_express_company/add',
    method: 'post',
    data
  })
}

export function editExpressCompany(data) {
  return request({
    url: api_prefix + 'shop_express_company/edit',
    method: 'put',
    data
  })
}

export function updateExpressCompany(data) {
  return request({
    url: api_prefix + 'shop_express_company/update',
    method: 'put',
    data
  })
}

export function deleteExpressCompany(ids) {
  return request({
    url: api_prefix + 'shop_express_company/delete',
    method: 'delete',
    data: { ids }
  })
}
