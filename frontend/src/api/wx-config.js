import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getTableTreeData(params) {
  return request({
    url: api_prefix + 'wx_config/table_tree',
    method: 'get',
    params
  })
}

export function getByID(id) {
  return request({
    url: api_prefix + 'wx_config/get_id',
    method: 'get',
    params: { id }
  })
}

export function getConfigMenu() {
  return request({
    url: api_prefix + 'wx_config/get_config_menu',
    method: 'get'
  })
}

export function getAllConfigParentNode() {
  return request({
    url: api_prefix + 'wx_config/get_all_parent',
    method: 'get'
  })
}

export function getChildrenCount(pid) {
  return request({
    url: api_prefix + 'wx_config/get_children_count',
    method: 'get',
    params: { pid }
  })
}

export function addConfig(data) {
  return request({
    url: api_prefix + 'wx_config/add',
    method: 'post',
    data
  })
}

export function editConfig(data) {
  return request({
    url: api_prefix + 'wx_config/edit',
    method: 'put',
    data
  })
}

export function updateConfig(data) {
  return request({
    url: api_prefix + 'wx_config/update',
    method: 'put',
    data
  })
}

export function deleteConfig(ids) {
  return request({
    url: api_prefix + 'wx_config/delete',
    method: 'delete',
    data: { ids }
  })
}

export function updateConfigMenuData(data) {
  return request({
    url: api_prefix + 'wx_config/commit_config',
    method: 'post',
    data
  })
}
