import request from '@/utils/request'

export function deleteUploadTempFile(data) {
  return request({
    url: 'upload/delete/file',
    method: 'delete',
    data
  })
}
