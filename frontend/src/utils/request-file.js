import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL, // url = base url + request url
  withCredentials: true, // send cookies when cross-domain requests
  crossDomain: true,
  timeout: 30000, // request timeout
  responseType: 'blob'
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent

    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers['Authorization'] = 'Bearer ' + getToken()
    }
    // console.log(config)

    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data
    const msg = res.msg || '请求失败'

    // if the custom code is not 20000, it is judged as an error.
    if (res.errorCode && res.errorCode !== 0) {
      Message({
        message: msg,
        type: 'error',
        duration: 5 * 1000
      })

      // 30000: Token已过期或者无效Token; 30001: Token不能为空;
      if (res.errorCode === 30000 || res.errorCode === 30001) {
        console.log('reset')

        // to re-login
        MessageBox.confirm('Token 已失效，请重新登陆', '确认登出', {
          confirmButtonText: '重新登陆',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        }).catch(() => {})
      }
      return Promise.reject(new Error(msg))
    } else {
      return response
    }
  },
  error => {
    // 由于下载文件时是使用了blob进行传输，所以需要进行转换后才能使用json
    const reader = new FileReader()
    reader.err_msg = error.msg || '请求失败'
    reader.readAsText(error.response.data, 'utf-8')
    reader.onload = function() {
      const res = JSON.parse(this.result)
      let msg = this.err_msg
      if (res && res.msg) {
        msg = res.msg || '请求失败'
      }
      Message({
        message: msg,
        type: 'error',
        duration: 5 * 1000
      })
      // 30000: Token已过期或者无效Token; 30001: Token不能为空;
      if (res.errorCode === 30000 || res.errorCode === 30001) {
        console.log('reset')

        // to re-login
        MessageBox.confirm('Token 已失效，请重新登陆', '确认登出', {
          confirmButtonText: '重新登陆',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        }).catch(() => {})
      }
    }
    // console.log('err', error, error.response) // for debug
    // const res = error.response.data
    // let msg = error.msg || '请求失败'
    // if (res && res.msg) {
    //   msg = res.msg || '请求失败'
    // }
    // Message({
    //   message: msg,
    //   type: 'error',
    //   duration: 5 * 1000
    // })
    // // 30000: Token已过期或者无效Token; 30001: Token不能为空;
    // if (res.errorCode === 30000 || res.errorCode === 30001) {
    //   console.log('reset')

    //   // to re-login
    //   MessageBox.confirm('Token 已失效，请重新登陆', '确认登出', {
    //     confirmButtonText: '重新登陆',
    //     cancelButtonText: '取消',
    //     type: 'warning'
    //   }).then(() => {
    //     store.dispatch('user/resetToken').then(() => {
    //       location.reload()
    //     })
    //   }).catch(() => {})
    // }
    return Promise.reject(error)
  }
)

export default service
