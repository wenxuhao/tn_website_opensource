import {
  SHOPPING_CART_STORAGE_KEY,
  ORDER_STORAGE_KEY
} from '../config.js'

/**
 * 添加商品数据到购物车缓存中
 */
export function addProductToShoppingCart(product) {
  // 判断商品是否为服务商品
  if (checkVirtualProduct(product)) {
    return false
  }
  
  // 获取缓存中先获取当前购物车的数据
  let data = getShoppingCartDataFromCache()
  
  data = checkProductIsExist(data, product)
  
  // 写入缓存
  try {
    uni.setStorageSync(SHOPPING_CART_STORAGE_KEY, data)
    return true
  }catch(e) {
    uni.showToast({
      icon: 'none',
      title: '添加数据失败'
    })
    return false
  }
}

/**
 * 更新购物车的缓存数据
 * @param {Object} shopping_cart_data
 */
export function updateShoppingCartDataToCache(shopping_cart_data) {
  // 写入缓存
  try {
    uni.setStorageSync(SHOPPING_CART_STORAGE_KEY, shopping_cart_data)
    return true
  }catch(e) {
    uni.showToast({
      icon: 'none',
      title: '添加数据失败'
    })
    return false
  }
}

/**
 * 从缓存中获取购物车的数据
 */
export function getShoppingCartDataFromCache() {
  try {
    const value = uni.getStorageSync(SHOPPING_CART_STORAGE_KEY)
    return value || []
  }catch(e) {
    return []
  }
}


/**
 * 添加商品列表数据到订单提交数据中
 */
export function addSubmitOrderProductToCache(order_product) {
  // 由于每一次添加到提交订单到商品都是新的，旧的不会保留，所以无需获取旧的数据，直接覆盖新的数据即可
  try {
    uni.setStorageSync(ORDER_STORAGE_KEY, order_product)
    return true
  } catch(e) {
    return false
  }
}

/**
 * 从缓存中获取提交订单的商品数据
 */
export function getSubmitOrderProductFromCache() {
  try {
    const value = uni.getStorageSync(ORDER_STORAGE_KEY)
    return value || []
  }catch(e) {
    return []
  }
}

// 检查商品是否存在
function checkProductIsExist(data, product) {
  // 判断当前添加的商品是否已经存在购物车中
  let product_index = -1
  // 查找对应的specs_id是否存在
  const is_find = data.some((item, index) => {
    if (item.id === product.specs_id) {
      product_index = index
      return true
    }
  })
  
  if (is_find) {
    // 能从购物车中查找到数据
    const product_stock = product.stock
    const count = data[product_index]['count']
    data[product_index]['count'] = (count + product.buy_num) > product_stock ? product_stock : count + product.buy_num
  } else {
    // 添加商品数据到数据中
    let product_data = {
      id: product.specs_id,
      product_id: product.product_id,
      virtual_product: product.virtual_product,
      title: product.product_title,
      property: product.property_name_arr,
      image: product.image,
      price: product.selling_price,
      stock: product.stock,
      count: product.buy_num,
      is_select: false
    }
    data.push(product_data)
  }
  
  return data
}

// 检查商品是否为服务商品，服务商品无法加入到购物车
function checkVirtualProduct(product) {
  if (product.virtual_product == 1) {
    uni.showToast({
      icon: 'none',
      title: '服务商品无法添加到购物车'
    })
    return true
  }
  return false
}
