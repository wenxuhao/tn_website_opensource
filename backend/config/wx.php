<?php

return [
    'login_url' => 'https://api.weixin.qq.com/sns/jscode2session?'.
        'appid=%s&secret=%s&js_code=%s&grant_type=authorization_code',  //微信获取openid的url地址
    'access_token_url' => 'https://api.weixin.qq.com/cgi-bin/token?'.
        'grant_type=client_credential&appid=%s&secret=%s',              //微信获取access_token的url地址
    'mp_code_url' => 'https://api.weixin.qq.com/wxa/getwxacodeunlimit'.
        '?access_token=%s',                                             //微信获取小程序码的url地址
    'daily_visit_thread_url' => 'https://api.weixin.qq.com/datacube/getweanalysisappiddailyvisittrend'.
        '?access_token=%s',                                             //获取用户访问小程序数据日趋势
    'daily_summary_url' => 'https://api.weixin.qq.com/datacube/getweanalysisappiddailysummarytrend'.
        '?access_token=%s',                                             //获取用户访问小程序数据概况
    'mp_send_subscribe_message_url' => 'https://api.weixin.qq.com/cgi-bin/message/subscribe/send'.
        '?access_token=%s',                                             // 微信小程序发送订阅消息
    'oc_send_template_message_url' => 'https://api.weixin.qq.com/cgi-bin/message/template/send'.
        '?access_token=%s',                                             // 微信小程序发送订阅消息
];
