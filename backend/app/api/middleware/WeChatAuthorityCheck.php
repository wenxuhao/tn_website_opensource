<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-09
 * Time: 13:53
 */

namespace app\api\middleware;

class WeChatAuthorityCheck
{
    public function handle($request, \Closure $next)
    {
        return $next($request);
    }
}