<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-12-03
 * Time: 16:00
 */

namespace app\api\middleware\mp\v1;

use app\common\exception\LotteryException;
use app\common\exception\ParameterException;
use app\common\model\Lottery as LotteryModel;

class CheckLotteryTime
{
    public function handle($request, \Closure $next)
    {
        // 判断是否当前抽奖是否存在或者开启
        if ($request->param('id')) {
            $status = LotteryModel::checkLotteryTimeRange($request->param('id'));
            switch ($status) {
                case -1:
                    break;
                case -2:
                    throw new LotteryException([
                        'code' => 400,
                        'errorCode' => 40404,
                        'msg' => '抽奖活动还未开始'
                    ]);
                    break;
                case -3:
                    throw new LotteryException([
                        'code' => 400,
                        'errorCode' => 40405,
                        'msg' => '抽奖活动已经结束'
                    ]);
                    break;
            }
        } else {
            throw new ParameterException();
        }

        return $next($request);
    }
}