<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-11
 * Time: 14:29
 */

namespace app\api\middleware\mp\v1;


use app\common\exception\ParameterException;
use app\common\exception\ShopException;
use app\common\model\ShopActivities;

class CheckShopActivities
{
    public function handle($request, \Closure $next)
    {
        // 判断是否当前活动是否存在或者开启
        if ($request->param('activities_id')) {
            if (!ShopActivities::checkActivitiesIsExist($request->param('activities_id'))) {
                throw new ShopException([
                    'code' => 404,
                    'errorCode' => 40204,
                    'msg' => '微信小商店订单活动已经结束或者不存在'
                ]);
            }
        } else {
            throw new ParameterException();
        }

        $request->product_specs_id = ShopActivities::getProductSpecsIDByActivitiesID($request->param('activities_id'));

        return $next($request);
    }
}