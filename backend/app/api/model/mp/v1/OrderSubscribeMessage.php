<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-10
 * Time: 09:29
 */

namespace app\api\model\mp\v1;


use app\common\service\WxMpSubscribeMessage;

class OrderSubscribeMessage extends WxMpSubscribeMessage
{
    /**
     * 发送订阅消息给用户
     * @param $type
     * @param $open_id
     * @param $data
     * @param string $page
     */
    public function sendOrderSubscribeMessage($type, $open_id, $data, $page = '')
    {
        switch ($type) {
            case 'create':
                $this->orderCreateSubscribe($data);
                break;
            case 'pay_timeout':
                $this->orderPayTimeoutSubscribe($data);
                break;
            case 'pay' :
                $this->orderPaySubscribe($data);
                break;
            case 'delivery':
                $this->orderDeliverySubscribe($data);
                break;
            case 'refund' :
                $this->orderRefundSubscribe($data);
                break;
        }

        $this->sendSubscribeMessage($open_id, $page);
    }

    /**
     * 填充订单下单成功的订阅消息数据
     * @param $data
     */
    private function orderCreateSubscribe($data)
    {
        $this->template_id = 'Ev6s1nV6j22jNtjgMIWKhnfsRX3UX6ofoC6geqQydMc';

        $this->data = [
            'character_string6' => [
                'value' => $data['order_no']
            ],
            'thing2' => [
                'value' => cut_str($data['title'], 15)
            ],
            'amount3' => [
                'value' => $data['amount']
            ],
            'date4' => [
                'value' => $data['create_time']
            ],
            'thing5' => [
                'value' => cut_str($data['address'], 15)
            ]
        ];
    }

    /**
     * 填充订单支付超时的订阅消息数据
     * @param $data
     */
    private function orderPayTimeoutSubscribe($data)
    {
        $this->template_id = 'JJRcSBXljaR7aRuGBiyY1r8vhB_aUUmirxrkCsYNQRk';

        $this->data = [
            'character_string1' => [
                'value' => $data['order_no']
            ],
            'thing3' => [
                'value' => cut_str($data['title'], 15)
            ],
            'time4' => [
                'value' => $data['timeout_time']
            ],
            'thing5' => [
                'value' => '订单准备超时，请尽快付款'
            ]
        ];
    }

    /**
     * 填充支付成功的订阅消息数据
     * @param $data
     */
    private function orderPaySubscribe($data)
    {
        $this->template_id = 'IwhmNHIdELTGvNTqlo4Tz1y-7ojU7GsZAkB0qVU34BY';

        $this->data = [
            'character_string3' => [
                'value' => $data['order_no']
            ],
            'thing5' => [
                'value' => cut_str($data['title'], 15)
            ],
            'amount1' => [
                'value' => $data['amount']
            ],
            'date2' => [
                'value' => $data['create_time']
            ]
        ];
    }

    /**
     * 填充商品发货的订阅消息数据
     * @param $data
     */
    private function orderDeliverySubscribe($data)
    {
        $this->template_id = '4wDuEywMFodW26lQEMuMPQpOyPHLd7Ofs0eAonyyrY8';

        $this->data = [
            'thing1' => [
                'value' => cut_str($data['title'], 15)
            ],
            'character_string2' => [
                'value' => cut_str($data['order_no'], 28)
            ],
            'thing4' => [
                'value' => strlen($data['express_company_name']) > 20 ? cut_str($data['express_company_name'], 15) : $data['express_company_name']
            ],
            'character_string5' => [
                'value' => cut_str($data['express_no'], 28)
            ],
            'date3' => [
                'value' => $data['delivery_time']
            ]
        ];
    }

    /**
     * 填充发送退款成功的订阅消息数据
     * @param $data
     */
    private function orderRefundSubscribe($data)
    {
        $this->template_id = 'PDkrF01llNsWjEQpCNwaYXsIvDEjlttRQCLWmwFR7Mk';

        $this->data = [
            'character_string3' => [
                'value' => $data['order_no']
            ],
            'thing2' => [
                'value' => cut_str($data['title'], 15)
            ],
            'thing6' => [
                'value' => cut_str($data['refund_desc'], 15)
            ],
            'amount1' => [
                'value' => $data['refund_amount']
            ],
            'date4' => [
                'value' => $data['refund_create_time']
            ]
        ];
    }
}