<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-03
 * Time: 09:14
 */

namespace app\api\controller\mp\v1;


use app\api\BaseController;
use app\common\model\Category as CategoryModel;

class Category extends BaseController
{
    /**
     * 获取对应顶级栏目id下的栏目数据
     * @http GET
     * @url /category/get_nav/:top_id
     * :top_id 顶级栏目id
     * @return \think\response\Json
     */
    public function getNavData()
    {
        $top_id = $this->request->param('top_id');

        $navData = CategoryModel::getChildrenNavByID($top_id);

        return tn_yes('获取对应栏目下的子栏目成功', ['data' => $navData]);
    }
}