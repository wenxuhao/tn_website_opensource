<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-02
 * Time: 16:18
 */

namespace app\api\controller\mp\v1;


use app\api\BaseController;
use app\api\model\mp\v1\Token as TokenModel;

class Token extends BaseController
{
    /**
     * 获取Token令牌
     * @http POST
     * @url /token/get
     * :code 登录后的微信小程序code
     * @return \think\response\Json
     */
    public function getToken()
    {
        $code = $this->request->post('code');

        // 获取token
        $token = TokenModel::getToken($code);

        return tn_yes('获取token成功', ['token' => $token]);
    }

    /**
     * 验证Token令牌
     * @http POST
     * @url /token/verify
     * :token 需要验证的token
     * @return \think\response\Json
     */
    public function verifyToken()
    {
        $token = $this->request->post('token');

        $result = TokenModel::verifyToken($token);

        return tn_yes('验证token成功', ['result' => $result]);
    }
}