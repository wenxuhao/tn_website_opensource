<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-03
 * Time: 10:55
 */

namespace app\api\controller\mp\v1;


use app\api\BaseController;
use app\api\model\mp\v1\Search as SearchModel;

class Search extends BaseController
{
    /**
     * 获取搜索的结果
     * @http GET
     * @url /search/get_result
     * :page 页码
     * :limit 每页数量
     * :keyword 关键词
     * @return \think\response\Json
     * @throws \app\common\exception\ParameterException
     * @throws \app\common\exception\ResultException
     */
    public function getList()
    {
        $params = $this->request->get(['page','limit','keyword']);

        $data = SearchModel::getPaginationListData($params);

        return tn_yes('获取搜索结果成功', ['data' => $data]);
    }
}