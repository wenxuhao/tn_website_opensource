<?php

return [
    // 别名或分组
    'alias' => [
        'check.api.authority' => \app\api\middleware\WeChatAuthorityCheck::class,
        'check.order.belongs.user' => \app\api\middleware\mp\v1\CheckOrderBelongsToUser::class,
        'check.business.operation' => \app\api\middleware\mp\v1\CheckOperationBusinessUser::class,
        'check.content.operation' => \app\api\middleware\mp\v1\CheckOperationContentUser::class,
        'check.mp_shop.activities' => \app\api\middleware\mp\v1\CheckMPShopActivities::class,
        'check.mp_shop.activities_time' => \app\api\middleware\mp\v1\CheckMPShopActivitiesTime::class,
        'check.shop.activities' => \app\api\middleware\mp\v1\CheckShopActivities::class,
        'check.shop.activities_time' => \app\api\middleware\mp\v1\CheckShopActivitiesTime::class,
        'check.lottery' => \app\api\middleware\mp\v1\CheckLottery::class,
        'check.lottery.time' => \app\api\middleware\mp\v1\CheckLotteryTime::class
    ],
];
