<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-03
 * Time: 16:25
 */

namespace app\common\model\traits;


use app\common\exception\ContentException;
use app\common\model\MpApiUserToken;

trait CaseContent
{
    /**
     * 获取指定数量的案例推荐数据
     * @param $limit
     * @return mixed
     */
    public static function getCaseRecommData($limit)
    {
        // 首先获取案例下所有的子栏目id
        $categoryAllID = static::getAllCategoryIDByContentType(static::case_category_id,'推荐的内容为空',40101);

        // 查找对应栏目id下指定数量的推荐内容
        $recommContent = static::where([['category_id','in',$categoryAllID],['recomm','=',1],['status','=',1]])
            ->field(['id','title','main_image'])
            ->order(['sort'=>'ASC','create_time'=>'DESC'])
            ->limit($limit)
            ->select();

        if ($recommContent->isEmpty()) {
            throw new ContentException([
                'msg' => '推荐的内容为空',
                'errorCode' => 40101,
            ]);
        }

        $recommContent->visible(['id','title','main_image']);

        return $recommContent;
    }

    /**
     * 获取最新的案例内容
     * @param $limit
     * @return mixed
     */
    public static function getCaseNewestData($limit)
    {
        $categoryAllID = static::getAllCategoryIDByContentType(static::case_category_id,'最新的内容为空',40102);

        // 查找对应栏目id下指定数量的推荐内容
        $newestContent = static::where([['category_id','in',$categoryAllID],['status','=',1]])
            ->field(['id','title','main_image'])
            ->order(['create_time'=>'DESC'])
            ->limit($limit)
            ->select();

        if ($newestContent->isEmpty()) {
            throw new ContentException([
                'msg' => '最新的内容为空',
                'errorCode' => 40102,
            ]);
        }

        $newestContent->visible(['id','title','main_image']);

        return $newestContent;
    }

    /**
     * 获取栏目的的分页内容
     * @param $params
     * @return array
     */
    public static function getCaseCategoryPaginate($params)
    {
        $categoryID = [];
        $categoryAllID = static::getAllCategoryIDByContentType(static::case_category_id,'当前栏目下内容为空',40103);

        // 判断是获取指定栏目还是全部的栏目下的数据
        if ($params['category_id'] == 0) {
            $categoryID = $categoryAllID;
        } else {
            if (in_array($params['category_id'],$categoryAllID) !== false) {
                $categoryID[] = $params['category_id'];
            } else {
                throw new ContentException([
                    'msg' => '当前栏目下内容为空',
                    'errorCode' => 40103,
                ]);
            }

        }

        $paginateData = static::where([['category_id','in',$categoryID],['status','=',1]])
            ->field(['id','title','main_image'])
            ->order(['create_time'=>'DESC'])
            ->paginate([
                'page' => $params['page'],
                'list_rows' => $params['limit'],
            ],true);

        if ($paginateData->isEmpty()) {
            throw new ContentException([
                'msg' => '当前栏目下内容为空',
                'errorCode' => 40103,
            ]);
        }

        $paginateData->visible(['id','title','main_image']);

        return $paginateData;
    }

    /**
     * 获取案例内容的详细信息
     * @param $id
     * @return mixed
     */
    public static function getCaseContentData($id)
    {

        $categoryAllID = static::getAllCategoryIDByContentType(static::case_category_id,'对应id的内容为空',40100);

        // 根据id获取对应的基本内容
        $contentData = static::with(['category', 'product'])
            ->where([['id','=',$id],['category_id','in',$categoryAllID],['status','=',1]])
            ->field(['id','title','main_image','model_id','table_id','category_id','product_id','view_count','like_count','share_count'])
            ->withAttr('table_data',function ($value,$data){
                return static::getTableData($data);
            })
            ->find();

        if (empty($contentData)) {
            throw new ContentException();
        }

        $contentData->append(['table_data']);
        $contentData = $contentData->toArray();

        // 获取商品价格的最低价和最高价
        $product = $contentData['product'];
        if ($product) {
            $price = array_column($product['specs'], 'selling_price');
            $min_price = min($price);
            $max_price = max($price);
            $contentData['product']['price'] = $min_price . '-' . $max_price;
        }

        return $contentData;
    }
}