<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-11-30
 * Time: 11:30
 */

namespace app\common\model\traits;

use app\common\model\LotteryPrize as LotteryPrizeModel;

trait HandleLotteryData
{
    /**
     * 分离提交的数据
     * @param $data
     * @param $lottery_id
     * @return array
     */
    private static function SeparationSubmitData($data, $lottery_id)
    {
        $prize_data = [];

//        $lottery_data['title'] = $data['title'];
//        $lottery_data['desc'] = $data['desc'];
//        $lottery_data['content'] = $data['content'];
//        $lottery_data['type'] = $data['type'];
//        $lottery_data['status'] = $data['status'];

        // 组合抽奖奖品数据
        foreach ($data['prize'] as $prize_key => $prize_value) {
            if (empty($prize_value['title'])) {
                continue ;
            }
            $prize_data[] = [
                'id' => $lottery_id . $prize_key . '1',
                'title' => $prize_value['title'],
                'image' => $prize_value['image'],
                'stock' => $prize_value['stock'],
                'rate' => $prize_value['rate']
            ];
        }

        return [
            'prize' => $prize_data
        ];
    }

    /**
     * 重组默认中奖用户数据(新旧数据处理)
     * @param $data
     * @return array
     */
    private static function recombinantDefaultUserData($data)
    {
        $lottery_id = $data['lottery_id'];

        // 取出旧的默认中奖用户数据
        $old_default_user = self::getDefaultUserByLotteryID($lottery_id)['default_user'];

        // 将用户id作为键，其他参数作为值
        $all_old_user_id = array_column($old_default_user, 'user_id');
        $old_user = [];
        foreach ($old_default_user as $old_user_key => $old_user_value) {
            $old_user[$old_user_value['user_id']] = [
                'lottery_prize_id' => $old_user_value['lottery_prize_id'],
                'receive_status' => $old_user_value['receive_status']
            ];
        }

        $default_data = [];

        foreach ($data['user'] as $user_item) {
            if (empty($user_item['user_id']) || empty($user_item['lottery_prize_id'])) {
                continue ;
            }
            $user_id = $user_item['user_id'];
//            $lottery_prize_id = $user_item['lottery_prize_id'];
//            $update_data = [
//                'user_id' => $user_id,
//                'lottery_prize_id' => $lottery_prize_id,
//                'receive_status' => $user_item['receive_status']
//            ];
//            if (in_array($user_item['user_id'], $all_old_user_id)) {
//                // 如果是旧已经存在默认用户，则使用旧的数据，不进行更新
//                $update_data['receive_status'] = $old_user[$user_id]['receive_status'];
//                if ($lottery_prize_id != $old_user[$user_id]['lottery_prize_id']) {
//                    $update_data['lottery_prize_id'] = $old_user[$user_id]['lottery_prize_id'];
//                }
//            }
//            $default_data[] = $update_data;

            if (!in_array($user_id, $all_old_user_id) ||
                (in_array($user_id, $all_old_user_id) && $old_user[$user_id]['receive_status'] == 0)
            ) {
                $default_data[] = [
                    'user_id' => $user_id,
                    'lottery_prize_id' => $user_item['lottery_prize_id'],
                    'receive_status' => $user_item['receive_status']
                ];
            } else {
                $default_data[] = [
                    'user_id' => $user_id,
                    'lottery_prize_id' => $old_user[$user_id]['lottery_prize_id'],
                    'receive_status' => $old_user[$user_id]['receive_status']
                ];
            }
        }

        return $default_data;
    }

    /**
     * 删除与当前抽奖旧的奖品数据
     * @param $lottery_id
     */
    private static function deleteOldPrize($lottery_id)
    {
        LotteryPrizeModel::where([['lottery_id','=',$lottery_id]])
            ->delete();
    }
}