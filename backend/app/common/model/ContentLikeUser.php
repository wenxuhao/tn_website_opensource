<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-29
 * Time: 21:03
 */

namespace app\common\model;


class ContentLikeUser extends BaseModel
{
    protected $hidden = ['update_time'];

    public function user()
    {
        return $this->belongsTo('WeChatUser','user_id','id')->bind(['nick_name','avatar_url']);
    }
}