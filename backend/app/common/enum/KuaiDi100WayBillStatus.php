<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-11-18
 * Time: 19:54
 */

namespace app\common\enum;


class KuaiDi100WayBillStatus
{
    // 在途
    const IN_THE_WAY = 0;

    // 揽收
    const TAKE_IT = 1;

    // 疑难
    const DIFFICULT = 2;

    // 签收
    const SIGN = 3;

    // 退签
    const SIGN_BACK = 4;

    // 派件
    const SEND = 5;

    // 退回
    const RETURN = 6;

    // 转单
    const TRANSFER_ORDER = 7;

    // 待清关
    const TO_BE_CLEARED = 10;

    // 清关中
    const CLEARANCE_IN = 11;

    // 已清关
    const CLEARANCE_HAS_MADE = 12;

    // 清关异常
    const CLEARANCE_EXCEPTIOM = 13;

    // 拒签
    const REFUSAL_SIGN = 14;
}