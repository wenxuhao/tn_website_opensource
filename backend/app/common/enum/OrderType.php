<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-02-16
 * Time: 19:02
 */

namespace app\common\enum;


class OrderType
{
    // 赞赏订单
    const Appreciate = 1;

    // 图鸟商店商品
    const Shop_Order = 2;
}