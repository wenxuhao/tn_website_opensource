<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-05
 * Time: 15:01
 */

namespace app\common\validate;


class Order extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'title' => 'require|max:150',
        'img_url' => 'max:255',
        'freight' => 'float|gt:0',
        'user_id' => 'require|number|gt:0',
        'order_no' => 'require|max:100',
        'prepay_id' => 'require|max:100',
        'amount' => 'require|egt:0',
        'refund_amount' => 'require|elt:amount',
        'refund_desc' => 'max:80',
        'type' => 'require|number|gt:0',
        'status' => 'require|number|gt:0',
        'body' => 'require',
        'total_fee' => 'require|float|egt:0',
        'notify_url' => 'require',
        'allow_pay_subscribe' => 'require|number',
        'allow_refund_subscribe' => 'require|number',
        'allow_submit_success_subscribe' => 'require|number',
        'allow_pay_timeout_subscribe' => 'require|number',
        'allow_delivery_subscribe' => 'require|number',
        'specs_data' => 'require|array',
        'address' => 'require|array',
        'notes' => 'max: 255',
        'refund_reason' => 'require|max:100',
        'express_company_id' => 'require|number|gt:0',
        'express_no' => 'require|max:100'
    ];

    protected $message = [
        'id.require' => 'id值不能为空',
        'id.number' => 'id值格式不正确',
        'id.gt' => 'id的值必须大于0',
        'title.require' => '订单标题不能为空',
        'title.max' => '订单标题最大长度不能超过150',
        'img_url.max' => '订单缩略图地址不能超过255',
        'freight.float' => '运费格式不正确',
        'freight.gt' => '运费格式不正确',
        'user_id.require' => 'user_id值不能为空',
        'user_id.number' => 'user_id值格式不正确',
        'user_id.gt' => 'user_id的值必须大于0',
        'order_no.require' => '订单编号不能为空',
        'order_no.max' => '订单编号最大不能超过100',
        'prepay_id.require' => '预支付id不能为空',
        'prepay_id.max' => '预支付id最大不能超过100',
        'amount.require' => '付款金额不能为空',
        'amount.egt' => '付款金额必须大于等于0',
        'refund_amount.require' => '退款金额不能为空',
        'refund_amount.elt' => '退款金额必须小于等于付款金额',
        'refund_desc.max' => '退款理由最大不能超过80',
        'type.require' => '类型值不能为空',
        'type.number' => '类型值格式不正确',
        'type.gt' => '类型的值必须大于0',
        'status.require' => '状态值不能为空',
        'status.number' => '状态值格式不正确',
        'status.gt' => '状态的值必须大于0',
        'body.require' => '商品描述不能为空',
        'total_fee.require' => '价格不能为空',
        'total_fee.float' => '价格格式不正确',
        'total_fee.egt' => '价格不能小于0',
        'notify_url.require' => '通知地址不能为空',
        'allow_pay_subscribe.require' => '是否允许支付通知不能为空',
        'allow_pay_subscribe.number' => '是否允许支付通知格式不正确',
        'allow_refund_subscribe.require' => '是否允许支付通知不能为空',
        'allow_refund_subscribe.number' => '是否允许支付通知格式不正确',
        'allow_submit_success_subscribe.require' => '是否允许下单成功通知不能为空',
        'allow_submit_success_subscribe.number' => '是否允许下单成功通知格式不正确',
        'allow_pay_timeout_subscribe.require' => '是否允许支付超时通知不能为空',
        'allow_pay_timeout_subscribe.number' => '是否允许支付超时通知格式不正确',
        'allow_delivery_subscribe.require' => '是否允许发货通知不能为空',
        'allow_delivery_subscribe.number' => '是否允许发货通知格式不正确',
        'specs_data.require' => '商品规格数据不能为空',
        'specs_data.array' => '商品规格格式不正确',
        'address.require' => '收货地址不能为空',
        'address.array' => '收货地址格式不正确',
        'notes.max' => '订单备注最大长度为255',
        'refund_reason.require' => '退款原因不能为空',
        'refund_reason.max' => '退款原因最大长度为100',
        'express_company_id.require' => '所属快递公司值不能为空',
        'express_company_id.number' => '所属快递公司值格式不正确',
        'express_company_id.gt' => '所属快递公司的值必须大于0',
        'express_no.require' => '快递编号不能为空',
        'express_no.max' => '快递编号最大长度为100'
    ];

    protected $scene = [
        'add' => ['title','img_url','freight','user_id','order_no','prepay_id','amount','allow_pay_subscribe','allow_refund_subscribe','allow_pay_timeout_subscribe','type','status'],
        'refund' => ['id','amount','refund_amount','refund_desc', 'notify_url'],
        'create' => ['body','total_fee','notify_url','allow_pay_subscribe','allow_refund_subscribe','allow_pay_timeout_subscribe'],
        'create_tn_shop' => ['specs_data','address','notes','allow_pay_subscribe','allow_submit_success_subscribe','allow_pay_timeout_subscribe'],
        'cancel' => ['order_no'],
        'delivery_subscribe' => ['order_no', 'allow_delivery_subscribe'],
        'application_refund_tn_shop' => ['id','refund_reason','allow_refund_subscribe'],
        'delivery_order' => ['id','express_company_id','express_no'],
        'serve_virtual_order' => ['id'],
        'refresh_express' => ['id'],
        'generate_all_data_excel' => ['type'],
        'generate_delivery_data_excel' => ['type'],
    ];
}