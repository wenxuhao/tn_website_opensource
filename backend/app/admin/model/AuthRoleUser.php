<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-28
 * Time: 09:51
 */

namespace app\admin\model;


use think\model\Pivot;

class AuthRoleUser extends Pivot
{
    protected $autoWriteTimestamp = false;
}