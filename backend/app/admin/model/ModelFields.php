<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-28
 * Time: 14:27
 */

namespace app\admin\model;


use app\common\model\BaseModel;

class ModelFields extends BaseModel
{
    protected $autoWriteTimestamp = false;

    protected static $have_values_type = [2,3,4];   //包含可选值的类型

    /**
     * 在保存多选值前将值进行判断和处理
     * @param $value
     * @param $data
     * @return string
     */
    public function setValuesAttr($value, $data)
    {
        if (empty($value) || !in_array($data['type'],self::$have_values_type)) {
            return '';
        } else {
            // 如果是需要用英文逗号进行数据分割的，将中文，变成英文，
            return convert_chinese_dot($value);
        }
    }

    /**
     * 在获取可选值前将其转换为数组
     * @param $value
     * @param $data
     * @return array|string
     */
    public function getValuesAttr($value, $data)
    {
        if (empty($value)) {
            return '';
        } else {
            if (in_array($data['type'],self::$have_values_type)) {
                return explode(',', $value);
            }
            return $value;
        }
    }
}