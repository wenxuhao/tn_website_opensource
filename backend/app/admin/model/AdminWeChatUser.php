<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-23
 * Time: 16:25
 */

namespace app\admin\model;


use think\model\Pivot;

class AdminWeChatUser extends Pivot
{
    protected $autoWriteTimestamp = false;
}