<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-18
 * Time: 10:14
 */

namespace app\admin\model;


use app\common\exception\HaveUseException;
use app\common\exception\ParameterException;
use app\common\model\BaseModel;
use app\common\validate\IDCollection;
use think\model\concern\SoftDelete;
use app\admin\validate\AtlasCategory as Validate;
use app\common\model\Atlas as AtlasModel;

class AtlasCategory extends BaseModel
{
    protected $hidden = ['create_time','update_time','delete_time'];

    use SoftDelete;
    protected $deleteTime = 'delete_time';

    /**
     * 获取图集类目分类的分页数据
     * @param array $params
     * @return \think\Paginator
     */
    public static function getPaginationList(array $params)
    {
        static::validatePaginationData($params);

        $static = new static();
        $static = $static->order('sort','asc');

        foreach ($params as $name => $value) {
            $value = trim($value);
            switch ($name) {
                case 'name':
                    if (!empty($value)) {
                        $like_text = '%' . $value . '%';
                        $static = $static->whereLike('name', $like_text);
                    }
                    break;
            }
        }

        return $static
            ->paginate([
                'page' => $params['page'],
                'list_rows' => $params['limit']
            ], false);
    }

    /**
     * 获取全部图集类目分类的名称
     * @return array|\think\Collection
     */
    public static function getAllCategoryName()
    {
        $data = static::where('status', '=', 1)
            ->field('id,name')
            ->order('sort','asc')
            ->select();

        $data = !empty($data) ? $data->toArray(): [];

        return $data;
    }

    /**
     * 添加图集类目分类信息
     * @param array $data
     * @return bool
     */
    public static function addAtlasCategory(array $data)
    {
        $validate = new Validate();
        if (!$validate->scene('add')->check($data)) {
            throw new ParameterException([
                'msg' => $validate->getError()
            ]);
        }

        $static = static::create($data);

        if (isset($static->id) && $static->id > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 编辑图集类目分类信息
     * @param array $data
     * @return bool
     */
    public static function editAtlasCategory(array $data)
    {
        $validate = new Validate();
        if (!$validate->scene('edit')->check($data)) {
            throw new ParameterException([
                'msg' => $validate->getError()
            ]);
        }

        $static = static::find($data['id']);
        $result = $static->allowField(['id','name','sort','status'])
            ->save($data);

        if ($result !== false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 删除指定id的图集类目分类信息
     * @param $ids
     * @return bool
     */
    public static function deleteAtlasCategory($ids)
    {
        $validate = new IDCollection();
        if (!$validate->check(['ids' => $ids])) {
            throw new ParameterException([
                'msg' => $validate->getError()
            ]);
        }

        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }

        $atlas_data = AtlasModel::where('category_id','in', $ids)
            ->count('id');

        if (!empty($atlas_data)) {
            throw new HaveUseException([
                'msg' => '当前图集分类下还有图片',
                'errorCode' => 10006
            ]);
        }

        if (static::destroy($ids) !== false) {
            return true;
        } else {
            return false;
        }
    }
}