<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-26
 * Time: 11:50
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\common\model\WeChatUser as WeChatUserModel;

class WeChatUser extends BaseController
{
    /**
     * 获取授权微信用户的分页数据
     * @http get
     * @url /wechat_user/list
     * @return \think\response\Json
     */
    public function getList()
    {
        $params = $this->request->get();

        $data = WeChatUserModel::getPaginationList($params);

        return tn_yes('获取微信用户成功', return_vue_element_admin_pagination_data($data));
    }

    /**
     * 根据用户名和OpenID获取微信用户信息
     * @http get
     * @url /wechat_user/find_by_nickname_openid
     * @return \think\response\Json
     */
    public function findUserWithNickNameOrOpenID()
    {
        $param = $this->request->get('param','');

        $data = WeChatUserModel::getUserInfoWithNickNameOrOpenID($param);

        return tn_yes('获取用户信息成功', ['data' => $data]);
    }

    /**
     * 更新授权微信用户的相关信息
     * @http put
     * @url /wechat_user/update
     * @return \think\response\Json
     */
    public function updateWeChatUser()
    {
        $this->checkPutUrl();

        $data = $this->request->put();

        $result = WeChatUserModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            WeChatUserModel::updateUserTokenStatus([
                $data['field'] => $data['value']
            ]);
            $this->request->log_content = '更新微信用户信息成功';
            return tn_yes('更新微信用户信息成功');
        }else {
            $this->request->log_content = '更新微信用户信息失败';
            return tn_no('更新微信用户信息失败');
        }
    }
}