<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-08
 * Time: 13:21
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\admin\model\SystemConfig as SystemConfigModel;

class SystemConfig extends BaseController
{
    /**
     * 获取表格树形数据
     * @http get
     * @url /system_config/table_tree
     * @return \think\response\Json
     */
    public function getTableTree()
    {
        $params = $this->request->get();

        $data = SystemConfigModel::getTableTreeData($params);

        return tn_yes('获取配置项Table树形数据成功', ['data' => $data]);
    }

    /**
     * 根据id获取配置对应的信息
     * @http get
     * @url /system_config/get_by_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id');

        $data = SystemConfigModel::getByID($id);

        return tn_yes('获取配置项信息成功',['data' => $data]);
    }

    /**
     * 获取配置菜单信息
     * @http get
     * @url /system_config/get_config_menu
     * @return \think\response\Json
     */
    public function getMenuData()
    {
        $data = SystemConfigModel::getConfigMenuData();

        return tn_yes('获取配置菜单信息成功', ['data' => $data]);
    }

    /**
     * 获取系统配置的所有父节点信息
     * @http get
     * @url /system_config/get_all_parent
     * @return \think\response\Json
     */
    public function getAllConfigParentNode()
    {
        $data = SystemConfigModel::getAllConfigParentNode();

        return tn_yes('获取配置项全部父节点信息成功',['data' => $data]);
    }

    /**
     * 获取指定父节点下的子节点数量
     * @http get
     * @url /system_config/get_children_count
     * @return \think\response\Json
     */
    public function getChildrenCount()
    {
        $pid = $this->request->get('pid',0);

        $count = SystemConfigModel::getChildrenCount($pid);

        return tn_yes('获取当前父节点的子节点数量成功',['count' => $count]);
    }

    /**
     * 添加系统配置项信息
     * @http post
     * @url /system_config/add
     * @return \think\response\Json
     */
    public function addConfig()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['pid','cn_name','en_name','values','tips','value','type','sort','status']);

        $result = SystemConfigModel::addConfig($data);

        if ($result) {
            $this->request->log_content = '添加系统配置项信息成功';
            return tn_yes('添加系统配置项信息成功');
        }else {
            $this->request->log_content = '添加系统配置项信息失败';
            return tn_no('添加系统配置项信息失败');
        }
    }

    /**
     * 编辑系统配置项信息
     * @http post
     * @url /system_config/edit
     * @return \think\response\Json
     */
    public function editConfig()
    {
        $this->checkPutUrl();

        $data = $this->request->put(['id','pid','cn_name','en_name','values','tips','value','type','sort','status']);

        $result = SystemConfigModel::editConfig($data);

        if ($result) {
            $this->request->log_content = '编辑系统配置项信息成功';
            return tn_yes('编辑系统配置项信息成功');
        }else {
            $this->request->log_content = '编辑系统配置项信息失败';
            return tn_no('编辑系统配置项信息失败');
        }
    }

    /**
     * 更新系统配置项信息
     * @http put
     * @url /system_config/update
     * @return \think\response\Json
     */
    public function updateConfig()
    {
        $this->checkPutUrl();

        // 获取put的数据
        $data = $this->request->put();

        $this->checkUpdateValidate($data);

        $result = SystemConfigModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            $this->request->log_content = '更新系统配置项信息成功';
            return tn_yes('更新系统配置项信息成功');
        }else {
            $this->request->log_content = '更新系统配置项信息失败';
            return tn_no('更新系统配置项信息失败');
        }
    }

    /**
     * 删除系统配置项信息
     * @http delete
     * @url /system_config/delete
     * @return \think\response\Json
     */
    public function deleteConfig()
    {
        $this->checkDeleteUrl();

        $ids = $this->request->delete('ids');

        $result = SystemConfigModel::delConfig($ids);

        if ($result) {
            $this->request->log_content = '删除系统配置项信息成功';
            return tn_yes('删除系统配置项信息成功');
        }else {
            $this->request->log_content = '删除系统配置项单信息失败';
            return tn_no('删除系统配置项单信息失败');
        }
    }

    /**
     * 更新系统配置信息
     * @http post
     * @url /system_config/commit_config
     * @return \think\response\Json
     */
    public function commitConfigData()
    {
        $this->checkPostUrl();

        $data = $this->request->post();

        $result = SystemConfigModel::commitConfigData($data);

        if ($result) {
            $this->request->log_content = '更新系统配置信息成功';
            return tn_yes('更新系统配置信息成功');
        }else {
            $this->request->log_content = '更新系统配置信息失败';
            return tn_no('更新系统配置信息失败');
        }
    }
}