<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-11-14
 * Time: 14:51
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\admin\model\ShopExpressCompany as ShopExpressCompanyModel;

class ShopExpressCompany extends BaseController
{
    /**
     * 获取商店快递公司分页列表数据
     * @http get
     * @url /shop_express_company/list
     * @return \think\response\Json
     */
    public function getList()
    {
        $params = $this->request->get();

        $data = ShopExpressCompanyModel::getPaginationList($params);

        return tn_yes('获取商店快递公司列表数据成功',return_vue_element_admin_pagination_data($data));
    }

    /**
     * 获取商店快递公司全部名称信息
     * @http get
     * @url /shop_express_company/get_all_name
     * @return \think\response\Json
     */
    public function getAllExpressCompanyName()
    {
        $name = $this->request->get('name', '');

        $data = ShopExpressCompanyModel::getAllCompanyName($name);

        return tn_yes('获取商店快递公司全部标题信息成功', ['data' => $data]);
    }

    /**
     * 根据id获取商店快递公司数据
     * @http get
     * @url /shop_express_company/get_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id',0);

        $data = ShopExpressCompanyModel::getDataWithID($id);

        return tn_yes('获取商店快递公司数据成功', ['data' => $data]);
    }

    /**
     * 添加快递公司
     * @http post
     * @url /shop_express_company/add
     * @return \think\response\Json
     */
    public function addExpressCompany()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['name','code']);

        $result = ShopExpressCompanyModel::addShopExpressCompany($data);

        if ($result) {
            $this->request->log_content = '添加商店快递公司成功';
            return tn_yes('添加商店快递公司成功');
        } else {
            $this->request->log_content = '添加商店快递公司失败';
            return tn_no('添加商店快递公司失败');
        }
    }

    /**
     * 通过Excel添加快递公司信息
     * @http post
     * @url /shop_express_company/add_excel
     * @return \think\response\Json
     */
    public function addExpressCompanyByExcel()
    {
        $this->checkPostUrl();

        // 获取上传的文件
        $file = $this->request->file('file');

        $result = ShopExpressCompanyModel::addShopExpressCompanyByExcel($file);

        if ($result) {
            $this->request->log_content = '通过Excel添加商店快递公司成功';
            return tn_yes('通过Excel添加商店快递公司成功');
        } else {
            $this->request->log_content = '通过Excel添加商店快递公司失败';
            return tn_no('通过Excel添加商店快递公司失败');
        }
    }

    /**
     * 编辑快递公司
     * @http post
     * @url /shop_express_company/edit
     * @return \think\response\Json
     */
    public function editExpressCompany()
    {
        $this->checkPutUrl();

        $data = $this->request->post(['id','name','code']);

        $result = ShopExpressCompanyModel::editShopExpressCompany($data);

        if ($result) {
            $this->request->log_content = '编辑商店快递公司成功';
            return tn_yes('编辑商店快递公司成功');
        } else {
            $this->request->log_content = '编辑商店快递公司失败';
            return tn_no('编辑商店快递公司失败');
        }
    }

    /**
     * 更新商店快递公司信息
     * @http PUT
     * @url /shop_express_company/update
     * @return \think\response\Json
     */
    public function updateExpressCompany()
    {
        $this->checkPutUrl();

        // 获取put的数据
        $data = $this->request->put();

        $this->checkUpdateValidate($data);

        $result = ShopExpressCompanyModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            $this->request->log_content = '更新商店快递公司信息成功';
            return tn_yes('更新商店快递公司信息成功');
        }else {
            $this->request->log_content = '更新商店快递公司信息失败';
            return tn_no('更新商店快递公司信息失败');
        }
    }

    /**
     * 删除指定的商店快递公司信息
     * @http DELETE
     * @url /shop_express_company/delete
     * @return \think\response\Json
     */
    public function deleteExpressCompany()
    {
        $this->checkDeleteUrl();

        $data = $this->request->delete('ids');

        $result = ShopExpressCompanyModel::deleteExpressCompany($data);

        if ($result) {
            $this->request->log_content = '删除商店快递公司信息成功';
            return tn_yes('删除商店快递公司信息成功');
        }else {
            $this->request->log_content = '删除商店快递公司信息失败';
            return tn_no('删除商店快递公司信息失败');
        }
    }
}