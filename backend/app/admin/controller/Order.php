<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-07
 * Time: 15:39
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\common\model\Order as OrderModel;

class Order extends BaseController
{
    /**
     * 获取订单分页数据信息
     * @http get
     * @url /order/list
     * @return \think\response\Json
     */
    public function getList()
    {
        $params = $this->request->get();

        $data = OrderModel::getPaginationList($params);

        return tn_yes('获取订单分页数据成功', return_vue_element_admin_pagination_data($data));
    }

    /**
     * 根据id获取订单信息
     * @http get
     * @url /order/get_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id');

        $data = OrderModel::getOrderBackendByID($id);

        return tn_yes('获取订单信息成功', ['data' => $data]);
    }

    /**
     * 得到全部订单数据的Excel表格
     * @http post
     * @url /order/get_all_order_excel
     */
    public function getAllOrderExcelData()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['type' => 0]);

        OrderModel::generateAllOrderDataExcel($data);
    }

    /**
     * 得到待发货的订单数据的Excel表格
     * @http post
     * @url /order/get_delivery_order_excel
     */
    public function getDeliveryOrderExcelData()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['type' => 0]);

        OrderModel::generateDeliveryOrderData($data);
    }

    /**
     * 对商店订单进行发货操作
     * @http post
     * @url /order/shop_delivery
     * @return \think\response\Json
     */
    public function deliveryShopOrder()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['id','express_company_id','express_no']);

        $result = OrderModel::deliveryShopOrder($data);

        if ($result) {
            $this->request->log_content = '订单发货成功';
            return tn_yes('订单发货成功');
        } else {
            $this->request->log_content = '订单发货失败';
            return tn_no('订单发货失败');
        }
    }

    /**
     * 对虚拟商品订单进行服务
     * @http post
     * @url /order/shop_virtual_product_serve
     * @return \think\response\Json
     */
    public function serveVirtualProductOrder()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['id']);

        $result = OrderModel::serveVirtualProductOrder($data);

        if ($result) {
            $this->request->log_content = '订单服务成功';
            return tn_yes('订单服务成功');
        } else {
            $this->request->log_content = '订单服务失败';
            return tn_no('订单服务失败');
        }
    }

    /**
     * 刷新商店订单的物流信息
     * @http put
     * @url /order/shop_refresh_express
     * @return \think\response\Json
     */
    public function refreshShopOrderExpress()
    {
        $this->checkPutUrl();

        $id = $this->request->put('id', 0);

        $data = OrderModel::refreshOrderExpressData($id);

        if ($data === -1) {
            $this->request->log_content = '更新商店订单物流信息失败';
            return tn_no('更新商店订单物流信息失败');
        } else if ($data === -2) {
            $this->request->log_content = '商店订单物流已经结束';
            return tn_no('商店订单物流已经结束');
        } else {
            $this->request->log_content = '更新商店订单物流信息成功';
            return tn_yes('更新商店订单物流信息成功', ['data' => $data]);
        }
    }

    /**
     * 发起订单退款信息
     * @http post
     * @url /order/refund
     * @return \think\response\Json
     */
    public function refundOrder()
    {
        $this->checkPostUrl();

        $params = $this->request->post(['id','amount','refund_amount','refund_desc']);
        $params['notify_url'] = 'https://website.tnkjapp.com/tn/wx/pay/notify_refund';

        $result = OrderModel::refundOrder($params);

        if ($result) {
            $this->request->log_content = '发起订单退款成功';
            return tn_yes('发起订单退款成功');
        }else {
            $this->request->log_content = '发起订单退款失败';
            return tn_no('发起订单退款失败');
        }
    }

    /**
     * 删除订单信息
     * @http delete
     * @url /order/delete
     * @return \think\response\Json
     */
    public function deleteOrder()
    {
        $this->checkDeleteUrl();

        $data = $this->request->delete('ids');

        $result = OrderModel::delByIDs($data);

        if ($result) {
            $this->request->log_content = '删除订单信息成功';
            return tn_yes('删除订单信息成功');
        }else {
            $this->request->log_content = '删除订单信息失败';
            return tn_no('删除订单信息失败');
        }
    }
}