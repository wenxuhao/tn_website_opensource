<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-11-30
 * Time: 10:11
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;

class Lottery extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'lottery_id' => 'require|number|gt:0',
        'user_id' => 'require|number|gt:0',
        'title' => 'require|max:60',
        'bg_image' => 'max: 255',
        'desc' => 'max:255',
        'content' => 'max:255',
        'is_default' => 'require|number|between:0,1',
        'start_time' => 'require|number',
        'end_time' => 'require|number|gt:start_time',
        'type' => 'require|number|between:0,1',
        'status' => 'require|number|between:0,1',
        'prize' => 'checkPrize',
        'default_user' => 'checkDefaultUser'
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'lottery_id.require' => '所属抽奖id不能为空',
        'lottery_id.number' => '所属抽奖id必须为整数',
        'lottery_id.gt' => '所属抽奖id必须大于0',
        'user_id.require' => '用户id不能为空',
        'user_id.number' => '用户id必须为整数',
        'user_id.gt' => '用户id必须大于0',
        'title.require' => '标题不能为空',
        'title.max' => '标题的最大长度为60',
        'bg_image.max' => '背景图片的地址最大长度为255',
        'desc.max' => '描述的最大长度不能超过255',
        'content.max' => '内容的最大长度不能超过255',
        'is_default.require' => '是否使用默认名单不能为空',
        'is_default.number' => '是否使用默认名单值格式错误',
        'is_default.between' => '是否使用默认名单请保持正常值',
        'start_time.require' => '活动开始时间不能为空',
        'start_time.number' => '活动开始时间格式不正确',
        'end_time.require' => '活动结束时间不能为空',
        'end_time.number' => '活动结束时间格式不正确',
        'end_time.gt' => '活动结束时间需要比开始时间晚',
        'type.require' => '类型不能为空',
        'type.number' => '类型值格式错误',
        'type.between' => '类型请保持正常值',
        'status.require' => '状态不能为空',
        'status.number' => '状态值格式错误',
        'status.between' => '状态请保持正常值',
    ];

    protected $scene = [
        'add' => ['title','bg_image','desc','content','is_default','start_time','end_time','type','status','prize'],
        'edit' => ['id','title','bg_image','desc','content','is_default','start_time','end_time','type','status','prize'],
        'default' => ['lottery_id','default_user'],
        'check_winning' => ['id', 'user_id'],
        'delete_winning' => ['lottery_id', 'user_id']
    ];

    /**
     * 检查抽奖活动奖品格式是否正确
     * @param $value
     * @param $rule
     * @return bool|string
     */
    public function checkPrize($value, $rule)
    {
        if (!empty($value)) {
            if (!is_array($value)) {
                return '抽奖活动奖品内容格式错误';
            }
            if (count($value) < 3) {
                return '抽奖奖项不能少于3项';
            }
            if (count($value) > 9) {
                return '抽奖奖项最多只能为9项';
            }
            foreach ($value as $item) {
                if (!isset($item['id']) || !isset($item['title']) || !isset($item['stock']) || !isset($item['rate'])) {
                    return '抽奖活动奖品内容参数错误';
                }
                if (mb_strlen($item['title']) > 60) {
                    return '抽奖活动奖品内容标题最大长度为60';
                }
            }
            // 取出全部rate的值
            $rate = array_column($value,'rate');
            // 判断中奖概率的值是否全为100，如果是，则中奖概率为平分
            $average_rate = array_filter($rate, function ($rate_item) {
                if ($rate_item == 100) {
                    return true;
                }
            });
            if (count($average_rate) != count($rate)) {
                // 判断中奖概率的总和是否为100
                // 计算rate的总和
                $rate_sum = array_sum($rate);
                if ($rate_sum != 100) {
                    return '奖品的中奖概率不为100，请重新输入中间概率的数值';
                }
            }
        } else {
            return '抽奖活动奖品不能为空';
        }

        return true;
    }

    /**
     * 检查抽奖活动默认名单格式是否正确
     * @param $value
     * @param $rule
     * @return bool|string
     */
    public function checkDefaultUser($value, $rule)
    {
        if (!empty($value)) {
            if (!is_array($value)) {
                return '抽奖活动默认名单格式错误';
            }
            foreach ($value as $item) {
                if (!isset($item['id']) || !isset($item['lottery_id']) || !isset($item['lottery_prize_id'])) {
                    return '抽奖活动默认名单参数错误';
                }
            }
        }

        return true;
    }
}