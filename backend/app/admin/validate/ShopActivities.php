<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-25
 * Time: 13:59
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;

class ShopActivities extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'title' => 'require|max:60',
        'main_image' => 'max: 255',
        'product_specs_id' => 'require|number|gt:0',
        'start_time' => 'require|number',
        'end_time' => 'require|number|gt:start_time',
        'sort' => 'require|number|gt:0',
        'status' => 'require|number|between:0,1'
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'title.require' => '活动标题不能为空',
        'title.max' => '活动标题最大长度为60',
        'main_image.max' => '主图链接的最大长度为255',
        'product_specs_id.require' => '商品属性id不能为空',
        'product_specs_id.number' => '商品属性id必须为整数',
        'product_specs_id.gt' => '商品属性id必须大于0',
        'start_time.require' => '活动开始时间不能为空',
        'start_time.number' => '活动开始时间格式不正确',
        'end_time.require' => '活动结束时间不能为空',
        'end_time.number' => '活动结束时间格式不正确',
        'end_time.gt' => '活动结束时间需要比开始时间晚',
        'sort.require' => '排序不能为空',
        'sort.number' => '排序的值必须为正整数',
        'sort.gt' => '排序的值必须大于0',
        'status.require' => '活动状态不能为空',
        'status.number' => '活动状态格式不正确',
        'status.between' => '活动状态格式不正确',
    ];

    protected $scene = [
        'add' => ['title', 'main_image', 'product_specs_id', 'start_time', 'end_time', 'sort', 'status'],
        'edit' => ['id','title', 'main_image', 'product_specs_id', 'start_time', 'end_time', 'sort', 'status']
    ];
}