<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-18
 * Time: 10:40
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;

class AtlasCategory extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'name' => 'require|max:60',
        'sort' => 'require|number|gt:0',
        'status' => 'require|number|between:0,1'
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'name.require' => '名称不能为空',
        'name.max' => '名称最大长度为60',
        'sort.require' => '排序不能为空',
        'sort.number' => '排序格式不正确',
        'sort.gt' => '排序格式不正确',
        'status.require' => '状态不能为空',
        'status.number' => '状态格式不正确',
        'status.between' => '状态格式不正确',
    ];

    protected $scene = [
        'add' => ['name', 'sort', 'status'],
        'edit' => ['id', 'name', 'sort', 'status']
    ];
}